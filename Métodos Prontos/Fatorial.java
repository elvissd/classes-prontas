/*Método que calcula Fatorial do modo normal e com recursividade*/
package MetodosProntos;

import java.util.Scanner;

public class Fatorial {
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int numero;
        
        do{
            System.out.print("Entre com um número: ");
            numero = input.nextInt();
            if(numero < 0){
                System.out.println("Número inválido! Entre novamente com um número positivo.");
            }
        }while(numero < 0);
        
        System.out.println("Fatorial de " + numero + "! = " + fatorialRecursivo(numero));
    }
    
    //Fatorial normal
    public static int fatorial(int num){
        //Caso o número seja 0 já retorna 1, pois 0! = 1
        if(num == 0){
            return 1;
        }
        
        int fat = 1; //Defini fat inicialmente igual a 1, pois asism não preciso ir até o 1.
        for (int i = num; i > 1; i--) {
            fat *= i;
        }
        return fat;
    }
    
    //Fatorial Recursivo
    public static int fatorialRecursivo(int num){
        if(num == 0){
            return 1;
        }
        return num * fatorialRecursivo(num-1);
    }
    
}